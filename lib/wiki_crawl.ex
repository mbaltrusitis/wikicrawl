defmodule WikiCrawl do
  @moduledoc """
  Documentation for `WikiCrawl`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> WikiCrawl.hello()
      :world

  """
  def hello do
    :world
  end

  def start do
    :ok
  end

  @doc """
  Start crawling a site using `url` as an entry_link
  """
  @spec crawl(url :: binary()) :: map()
  def crawl url do
    crawl(url, "")
  end

  @spec crawl(url :: binary, source :: binary(), map()) :: map()
  def crawl(url, origin, visited_links \\ %{})

  def crawl(url, "", visited_links) do
    case get_page(url) do
      {:ok, resp} ->
        Floki.parse_document!(resp.body)
        |> get_links()
        |> Enum.reduce(visited_links, fn link, acc -> crawl(link, url, acc) end)
      {:error, exception} ->
        dbg()
        IO.puts(:stderr, "Request error")
        %{}
    end
  end

  def crawl(url, source, visited_links) do
    case validate_uri(url) do
      {:ok, {false, :relative}} ->
        fixed_url = fixup_relative_url(url, source)
        IO.puts("[INFO]: Fixed-up relative link. From: #{url} To: #{fixed_url}")
        crawl(fixed_url, source, visited_links)
      {:ok, {false, :localhost}} ->
        IO.puts("[INFO]: Local address. Skipping #{url}.")
        visited_links
      {:ok, {true, _}} -> true
      {:error, msg} ->
        IO.puts(:stderr, "[ERROR]: #{msg}")
        visited_links
    end

    if url_equivalent_to_source?(url, source) or Map.has_key?(visited_links, url) do
      update_visited_urls(url, source, 200, visited_links)
    else
      case get_page(url) do
        {:ok, resp} ->
          visited_links = update_visited_urls(url, source, resp.status, visited_links)
          new_links =
            Floki.parse_document!(resp.body)
            |> get_links()
          Enum.reduce(new_links, visited_links, fn link, acc -> crawl(link, url, acc) end)
        {:error, _reason} ->
          IO.puts(:stderr, "Failed to get page.")
          visited_links
      end
    end
  end

  def url_equivalent_to_source?(url, source) do
    String.equivalent?(url, source)
  end

  def fixup_relative_url(url, source) do
    # host absolute path
    if String.starts_with?(url, "/") do
      host = Application.get_env(:wiki_crawl, :base_host)
      "https://#{host}#{url}"
    # relative path
    else
      "#{source}/#{url}"
    end
  end

  @spec update_visited_urls(url :: binary(), source :: binary(), status :: integer(), visited_urls :: map()) :: map()
  def update_visited_urls(url, source, status, visited_urls) do
    if Map.has_key?(visited_urls, url) do
      {_val, map} = Map.get_and_update(
        visited_urls,
        url,
        fn current_value ->
          {
            current_value,
            %{
            status: current_value.status,
            locations: Enum.reverse([source | current_value.locations ])
            }
          }
      end)
      map
    else
      Map.put_new(
        visited_urls,
        url,
        %{
          status: status,
          locations: [source]
        })
    end
  end

  @doc """
  Make a certain type of request depending on the `url` host.

  For "internal links" who's host is the same as `@base_host`, make a GET request.

  For "external links" who's host does not match `@base_host` send a HEAD request.
  """
  def get_page(url) do
    base_host = Application.get_env(:wiki_crawl, :base_host)
    if String.contains?(url, base_host) do
      IO.puts("get: #{url}")
      get_page(url, :get)
    else
      IO.puts("head: #{url}")
      get_page(url, :head)
    end
  end

  def get_page(url, :head) do
    case Req.head(url) do
      {:error, exception} -> {:error, %{msg: "request error", exception: exception}}
      {:ok, resp} -> {:ok, resp}
    end
  end

  def get_page(url, :get) do
    case Req.get(url) do
      {:error, exception} -> {:error, %{msg: "request error", exception: exception}}
      {:ok, resp} -> {:ok, resp}
    end
  end

  @doc """
  Test if a given URI is requestable
  """
  def validate_uri(uri) do
    cond do
      localhost? uri ->
        {:ok, {false, :localhost}}
      relative_path? uri ->
        {:ok, {false, :relative}}
      String.match?(uri, ~r"^(http)s?:\/\/.*$") ->
        {:ok, {true, nil}}
      true -> {:error, :validation_failure}
    end
  end

  @doc """
  Filter-out links that do not point to other webpages.
  """
  def filter_nonpage_links(html) do
    html
    |> Floki.filter_out("a[href^=\"mailto:\"]") # email links
    |> Floki.filter_out("a[href^=\"tel:\"]") # telephone numbers
    |> Floki.filter_out("a[href^=\"sms:\"]") # telephone numbers
    |> Floki.filter_out("a[href^=\"#\"]") # anchor links
    |> Floki.filter_out("a[href^=\"relref\"]") # relref
    |> Floki.filter_out("a[download]") # download links
  end

  @spec localhost?(binary()) :: boolean()
  @doc """
  Tests if url host is local IP address
  """
  def localhost?(url) do
    String.match?(url, ~r"^(?'scheme'http[s]?:\/\/)(?'host'0|10|172|192|localhost[.]?).*$")
  end

  @doc """
  Test if a given string is a relative path
  """
  def relative_path?(url) do
    String.match?(url, ~r"^(?!www\.|(?:http|ftp)s?://|[A-Za-z]:\\|//).*")
  end

  def process_link do
    :ok
  end

  @doc """
  Given an HTML tree return all of the anchor elements
  """
  def filter_anchor_tags(html) do
      html
      |> Floki.find("a")
  end

  @doc """
  Return valid internal and external page links.
  """
  def get_links(html, opts \\ [])

  def get_links(html, uniq: true) do
    html
    |> filter_anchor_tags()
    |> filter_nonpage_links()
    |> Floki.attribute("href")
    |> Enum.uniq()
  end

  def get_links(html, _opts) do
    html
    |> filter_anchor_tags()
    |> filter_nonpage_links()
    |> Floki.attribute("href")
  end


  # @spec put_parse_response_body(Req.Response.t()) :: Req.Response.t()
  # @doc """
  # Put the parsed HTML body into private key space.
  # """
  # def put_parse_response_body(response) do
  #   case Floki.parse_document(response.body) do
  #     {:ok, parsed_body} ->
  #       Req.Response.put_private(response, :wc_parsed_body, parsed_body)
  #     {:error, reason} ->
  #       IO.puts("[ERROR]: Could not parse body. Reason:\n#{reason}")
  #       Req.Response.put_private(response, :wc_parsed_body, [])
  #   end
  # end

end
