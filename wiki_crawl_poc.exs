# Run as: iex --dot-iex path/to/notebook.exs

# Title: wiki crawl poc

Mix.install([
  {:floki, "~> 0.35.3"},
  {:req, "~> 0.4.8"},
  {:kino_explorer, "~> 0.1.4"},
  {:explorer, "~> 0.8.0"},
  {:kino, "~> 0.12.3"}
])

# ── Section ──

base_host = "https://wiki.mesh.nycmesh.net"
base_url = "#{base_host}/books"

defmodule Crawler do
  @base_host "https://wiki.mesh.nycmesh.net"

  def get_links(html) do
    html
    |> Floki.parse_document!()
    |> Floki.find("a")
    |> Floki.attribute("href")
  end

  def get_page(url, :get) do
    case Req.get(url) do
      {:error, exception} -> {:ok, {"request error", []}}
      {:ok, resp} -> {:ok, {resp.status, resp.body}}
    end
  end

  def get_page(url, :head) do
    case Req.head(url) do
      {:error, exception} -> {:ok, {"request error", []}}
      {:ok, resp} -> {:ok, {resp.status, resp.body}}
    end
  end

  def remove_anchor_links(html) do
    html
    |> Enum.filter(fn url -> not String.starts_with?(url, "#") end) # remove anchor links
  end

  def is_valid_url?(url) do
      cond do
        String.starts_with?(url, "relref") or
        String.starts_with?(url, "https://10.") or
        String.starts_with?(url, "http://10.") or
        String.starts_with?(url, "https://192.") or
        String.starts_with?(url, "http://192.") or
        String.starts_with?(url, "https://172.") or
        String.starts_with?(url, "http://172.") or
        String.starts_with?(url, "mailto") ->
          false
        true ->
          true
      end
  end

  def crawl(url) do
    crawl(url, "origin URL", %{})
  end

  def crawl(url, origin, visited_links) do
    cond do
      not is_valid_url?(url) ->
        IO.puts("invalid url: #{url}")
        Map.put_new(visited_links, url, %{status: "error: bad link(#{url})", locations: [origin]})
      String.equivalent?(url, origin) ->
        visited_links
      Map.has_key?(visited_links, url) ->
        Map.update!(visited_links, url, fn %{status: status, locations: locations} ->
          %{status: status, locations: [url | locations]}
        end)
      String.starts_with?(url, ".") ->
        relative_url = "#{origin}/#{url}"
        crawl(relative_url, origin, visited_links)
      String.starts_with?(url, "/") ->
        relative_url = "#{@base_host}/#{url}"
        crawl(relative_url, origin, visited_links)
      not String.starts_with?(url, @base_host) ->
        IO.puts("external link: #{url}")
        {:ok, {status, _body}} = get_page(url, :head)
        Map.put_new(visited_links, url, %{status: status, locations: [origin]})
      true ->
        IO.puts("new link: #{url}")
        {:ok, {status, body}} = get_page(url, :get)
        visited_links = Map.put_new(visited_links, url, %{status: status, locations: [origin]})

        body
        |> get_links()
        |> remove_anchor_links()
        |> Enum.uniq()
        |> Enum.reduce(visited_links, fn link, acc -> crawl(link, url, acc) end)
    end
  end

  def main(start_url) do
    IO.puts("starting")
    links = crawl(start_url)
    IO.puts("ending")
    IO.inspect(links)
    flattened = ["url,status,origin\n"]
    for {key, %{status: status, origin: origin}} <- links do
      Enum.reduce(origin, for loc <- origin do
        flattened = [ "#{key},#{status},#{loc}\n" | flattened ]
      end
    end
    Enum.reverse(flattened)
  end
end

link_data = Crawler.main(base_url)
File.write!("link-out.csv", link_data)
IO.inspect(link_data)

# link_data = [
#   %{"url" => "www.com", "status" => "200", "location" => "blue" },
#   %{"url" => "www.com", "status" => "200", "location" => "red" },
#   %{"url" => "www.com", "status" => "200", "location" => "green" }
# ]

# Explorer.DataFrame.new(
#   link_data
# )
