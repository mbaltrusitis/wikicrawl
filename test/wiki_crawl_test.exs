defmodule WikiCrawlTest do
  use ExUnit.Case
  doctest WikiCrawl

  test "greets the world" do
    assert WikiCrawl.hello() == :world
  end

  # Tests:
  # - making the right type of request
  # - parsing the page in the private key space
  # - getting the right set of links
  # - returning the right set of data

  describe "test get_page/1" do
    test "makes GET request for internal link" do
      true
    end

    test "makes HEAD request to external link" do
      true
    end
  end

  # describe "test put_parse_response_body/1" do
  #   test "parsed html is put into `wc_parsed_body`" do
  #     resp =
  #       WikiCrawl.HtmlFixtures.basic()
  #       |> WikiCrawl.put_parse_response_body()
  #     assert Map.has_key?(resp.private, :wc_parsed_body)
  #   end
  # end

  describe "test get_links/1" do
    test "returns links from basic webpage html" do
      links =
        WikiCrawl.HtmlFixtures.basic(:html)
        |> WikiCrawl.get_links()
      assert length(links) == 2
    end

    test "returns one valid link from bad_links" do
      links =
        WikiCrawl.HtmlFixtures.bad_links(:html)
        |> WikiCrawl.get_links()
      assert length(links) == 1
    end
  end

  describe "test localhost?/1" do
    test "localhost returns true" do
      assert WikiCrawl.localhost?("https://localhost/some/path?foo=true")
      assert WikiCrawl.localhost?("http://localhost/some/path?foo=true")
    end

    test "local IPs return true" do
      assert WikiCrawl.localhost?("http://10.111.222.333:8000/path")
      assert WikiCrawl.localhost?("https://192.111.222.333:8000/path")
      assert WikiCrawl.localhost?("http://172.111.222.333/path")
      assert WikiCrawl.localhost?("https://0.0.0.0:8000/path")
    end

    test "non-local IP returns false" do
      refute WikiCrawl.localhost?("https://foo.com/some/path?foo=true")
      refute WikiCrawl.localhost?("http://bar.com/")
    end
  end

  describe "test relative_path?/1" do
    test "relative path value is true" do
      assert WikiCrawl.relative_path? "../relative/path"
      assert WikiCrawl.relative_path? "./relative/path"
      assert WikiCrawl.relative_path? "/relative/path"
      assert WikiCrawl.relative_path? "relative/path"
    end
  end

  describe "filter_nonpage_links" do
    test "returns only valid page links" do
      filtered_links =
        WikiCrawl.HtmlFixtures.bad_links(:html)
        |> WikiCrawl.filter_anchor_tags()
        |> WikiCrawl.filter_nonpage_links()
      assert length(filtered_links) == 1
    end
  end

  describe "validate_uri/1" do
    test "returns successfil for a valid link" do
      {:ok, {true, nil}} = WikiCrawl.validate_uri("https://www.wiki.com")
      {:ok, {true, nil}}
    end

    test "returns relative status for link" do
      {:ok, {false, :relative}} = WikiCrawl.validate_uri("relative/path")
      {:ok, {false, :relative}} = WikiCrawl.validate_uri("./relative/path")
      {:ok, {false, :relative}} = WikiCrawl.validate_uri("/origin/relative")
      {:ok, {false, :relative}} = WikiCrawl.validate_uri("relative")
    end

    test "returns local status for localhost/IPs" do
      {:ok, {false, :localhost}} = WikiCrawl.validate_uri("http://0.1.2.3:1234/some/path")
      {:ok, {false, :localhost}} = WikiCrawl.validate_uri("https://192.11.22.33:1234/some/path")
    end

    test "returns a validation failure for bad link" do
      {:error, :validation_failure} = WikiCrawl.validate_uri("www.bad-value.com")
    end
  end

  describe "update_visited_urls/4" do
    test "updates existing key" do
      url = "https://www.foo.com"
      origin = "https://www.biz.com"
      visited_links = %{url => %{status: 200, locations: ["https://www.bar.com"]}}
      updated_visited = WikiCrawl.update_visited_urls(url, origin, 200, visited_links)
      assert Map.get(updated_visited, url).status == 200
      assert Map.get(updated_visited, url).locations == ["https://www.bar.com", "https://www.biz.com"]
    end

    test "adds unseen key" do
      url = "https://www.target.com"
      origin = "https://origin.com"
      visited_links = %{"https://www.some-link.com" => %{status: 200, locations: ["https://www.bar.com"]}}
      updated_visited = WikiCrawl.update_visited_urls(url, origin, 200, visited_links)
      assert Map.has_key?(updated_visited, url)
      assert Map.get(updated_visited, url).status == 200
      assert Map.get(updated_visited, url).locations == [origin]
    end
  end

  describe "fixup_relative_url/2" do
    test "absolute url has host prefixed" do
      base_host = Application.get_env(:wiki_crawl, :base_host)
      abs_path = "/absolute/path"
      fixed_url = WikiCrawl.fixup_relative_url(abs_path, "https://www.foo.com")
      assert fixed_url == base_host <> abs_path
    end

    test "relative url has path prefixed" do
      src_url = "https://www.foo.com/here"
      rel_path = "../relative/path"
      fixed_url = WikiCrawl.fixup_relative_url(rel_path, src_url)
      assert fixed_url == src_url <> "/" <> rel_path
    end
  end
end
