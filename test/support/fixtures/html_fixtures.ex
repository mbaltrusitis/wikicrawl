defmodule WikiCrawl.HtmlFixtures do
  @moduledoc false

  @doc """
  Basic HTML fixture returning 2 valid anchor tags.
  """
  def basic do
    body = """
    <!DOCTYPE HTML>
    <html>
      <head>
        <title>Fixture Page Title</title>
      </head>
      <body>
        <h1>Lorem ipsum dolor</h1>
        Sit amet, qui minim labore <a href="http://www.fixturesite.com/123">adipisicing minim</a> sint cillum sint consectetur cupidatat.
        Lorem ipsum dolor sit amet, officia excepteur <a href="http://www.fixturesite.com/456">ex fugiat reprehenderit</a> enim labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet. Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident. Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa et culpa duis.
      </body>
    </html>
    """
    Req.Response.new(body: body)
  end

  def basic(:html) do
    resp = basic()
    Floki.parse_document!(resp.body)
  end


  def bad_links do
    body = """
    <!DOCTYPE HTML>
    <html>
      <head>
        <title>Fixture Page Title</title>
      </head>
      <body>
        <h1>Lorem ipsum dolor</h1>
        <ul>
          <li><a href="mailto:user@domain.com">Email me.</a></li>
          <li><a href="tel:18005555555">Call me.</a></li>
          <li><a href="sms:18005555555">Text me.</a></li>
          <li><a href="#some-header">Jump to header.</a></li>
          <li><a href="relref"></a></li>
          <li><a href="some-file.txt" download=file.txt></a></li>
          <li><a href="http://www.wikipedia.org">Wikipedia.org</a></li>
        </ul>
        <h1 id="some-header">Some header</h1>
        <p>Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet. Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident. Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa et culpa duis.</p>
      </body>
    </html>
    """
    Req.Response.new(body: body)
  end

  def bad_links(:html) do
    resp = bad_links()
    Floki.parse_document!(resp.body)
  end

  def relative_links do
    body = """
    <!DOCTYPE HTML>
    <html>
      <head>
        <title>Fixture Page Title</title>
      </head>
      <body>
        <h1>Lorem ipsum dolor</h1>
        <ul>
          <li><a href="./relativeLink">Directory-relative URL.</a></li>
          <li><a href="relativeLink">Another directory-relative URL.</a></li>
          <li><a href="/absoluteLink">Origin-relative URL.</a></li>
        </ul>
        <h1 id="some-header">Some header</h1>
        <p>Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet. Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident. Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa et culpa duis.</p>
      </body>
    </html>
    """
    Req.Response.new(body: body)
  end
end
